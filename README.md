# nyctaxis_2020
The data exploration notebook can be accessed in file **Final_project_notebook_3.ipynb**  

Unfortunately we have had difficulties deploying our website due to some files being too large. However, the screen grabs of the website can be seen in the **Website screenshots** folder. 



In order to run the website locally please clone this repository and run the following from the main folder:

python3 -m venv venv  
source venv/bin/activate  (for mac users)  
pip install numpy  
pip install pandas  
pip install wheel  
pip install sklearn  
pip install dash  
pip install dash-bootstrap-components  

python app.py  
